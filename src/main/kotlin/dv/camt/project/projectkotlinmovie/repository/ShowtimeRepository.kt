package dv.camt.project.projectkotlinmovie.repository

import dv.camt.project.projectkotlinmovie.entity.Showtime
import org.springframework.data.repository.CrudRepository
import java.util.*

interface ShowtimeRepository : CrudRepository<Showtime, Long> {
 fun findByMovie_Id(id: Long): List<Showtime>
 fun findShowtimeById(id: Long): Showtime
}