package dv.camt.project.projectkotlinmovie.repository

import dv.camt.project.projectkotlinmovie.entity.Booking
import org.springframework.data.repository.CrudRepository

interface BookingRepository : CrudRepository<Booking, Long> {
    fun findBookingByUser_Id(id: Long): List<Booking>

}