package dv.camt.project.projectkotlinmovie.repository

import dv.camt.project.projectkotlinmovie.entity.SelectedSeat
import org.springframework.data.repository.CrudRepository

interface SelectedSeatRepository : CrudRepository<SelectedSeat, Long> {

}