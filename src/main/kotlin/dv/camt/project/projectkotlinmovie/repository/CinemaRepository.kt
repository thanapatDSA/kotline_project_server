package dv.camt.project.projectkotlinmovie.repository

import dv.camt.project.projectkotlinmovie.entity.Cinema
import org.springframework.data.repository.CrudRepository

interface CinemaRepository : CrudRepository<Cinema,Long>