package dv.camt.project.projectkotlinmovie.repository

import dv.camt.project.projectkotlinmovie.entity.Movie
import org.springframework.data.repository.CrudRepository

interface MoviesRepository : CrudRepository<Movie, Long> {
    fun findMovieById(id: Long): Movie
}