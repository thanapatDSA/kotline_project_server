package dv.camt.project.projectkotlinmovie.repository

import dv.camt.project.projectkotlinmovie.entity.SeatShowtime
import org.springframework.data.repository.CrudRepository

interface SeatShowtimeRepository : CrudRepository<SeatShowtime, Long> {

}