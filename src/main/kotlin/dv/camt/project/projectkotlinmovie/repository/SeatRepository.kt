package dv.camt.project.projectkotlinmovie.repository

import dv.camt.project.projectkotlinmovie.entity.Seat
import org.springframework.data.repository.CrudRepository

interface SeatRepository : CrudRepository<Seat,Long>