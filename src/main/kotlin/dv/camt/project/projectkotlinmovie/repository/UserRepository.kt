package dv.camt.project.projectkotlinmovie.repository


import dv.camt.project.projectkotlinmovie.entity.User
import org.springframework.data.repository.CrudRepository

interface UserRepository : CrudRepository<User, Long> {
    fun save(user: User): User
    fun findUserById(id: Long?): User
    fun findUserByEmail(email: String?): User
}