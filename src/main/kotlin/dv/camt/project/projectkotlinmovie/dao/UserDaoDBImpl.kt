package dv.camt.project.projectkotlinmovie.dao

import dv.camt.project.projectkotlinmovie.entity.User
import dv.camt.project.projectkotlinmovie.entity.dto.UserDto
import dv.camt.project.projectkotlinmovie.repository.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository

@Profile("db")
@Repository

class UserDaoDBImpl:UserDao{
    override fun findUserByEmail(email: String?): User {
        return userRepository.findUserByEmail(email)
    }

    override fun findUserById(id: Long?): User {
        return userRepository.findUserById(id)
    }

    override fun save(user: User): User {
        return userRepository.save(user)
    }

    @Autowired
    lateinit var userRepository: UserRepository
}