package dv.camt.project.projectkotlinmovie.dao

import dv.camt.project.projectkotlinmovie.entity.Booking
import dv.camt.project.projectkotlinmovie.entity.User
import dv.camt.project.projectkotlinmovie.repository.BookingRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class BookingDaoImpl:BookingDao{
    override fun getBooking(user: User): List<Booking> {
        return bookingRepository.findBookingByUser_Id(user.id!!)
    }

    override fun save(book: Booking): Booking {
        return bookingRepository.save(book)
    }

    @Autowired
    lateinit var bookingRepository: BookingRepository
}