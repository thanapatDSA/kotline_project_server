package dv.camt.project.projectkotlinmovie.dao

import dv.camt.project.projectkotlinmovie.entity.SeatShowtime

interface SeatShowtimeDao {
    fun findById(id: Long?): SeatShowtime
    fun save(seat: SeatShowtime): SeatShowtime
}