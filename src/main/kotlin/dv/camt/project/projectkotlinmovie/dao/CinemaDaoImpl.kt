package dv.camt.project.projectkotlinmovie.dao

import dv.camt.project.projectkotlinmovie.entity.Cinema
import dv.camt.project.projectkotlinmovie.repository.CinemaRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class CinemaDaoImpl: CinemaDao{
    override fun getAllCinema(): List<Cinema> {
        return cinemaRepository.findAll().filterIsInstance(Cinema::class.java)
    }

    @Autowired
    lateinit var cinemaRepository: CinemaRepository

}