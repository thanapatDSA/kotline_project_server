package dv.camt.project.projectkotlinmovie.dao

import dv.camt.project.projectkotlinmovie.entity.User
import dv.camt.project.projectkotlinmovie.entity.dto.UserDto

interface UserDao {
    fun save(user: User): User
    fun findUserById(id: Long?): User
    fun findUserByEmail(email: String?): User
}