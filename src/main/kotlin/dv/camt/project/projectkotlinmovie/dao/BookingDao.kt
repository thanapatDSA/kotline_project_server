package dv.camt.project.projectkotlinmovie.dao

import dv.camt.project.projectkotlinmovie.entity.Booking
import dv.camt.project.projectkotlinmovie.entity.User

interface BookingDao {
    fun save(book: Booking): Booking
    fun getBooking(user: User): List<Booking>

}