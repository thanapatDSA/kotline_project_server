package dv.camt.project.projectkotlinmovie.dao

import dv.camt.project.projectkotlinmovie.entity.Showtime

interface ShowtimeDao {
    fun getShowTimeByMovieId(id: Long): List<Showtime>
    fun getShowTimeById(id: Long): Showtime
    fun getAllShowtimes(): List<Showtime>

}