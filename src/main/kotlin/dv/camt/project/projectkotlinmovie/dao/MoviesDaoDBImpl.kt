package dv.camt.project.projectkotlinmovie.dao

import dv.camt.project.projectkotlinmovie.entity.Movie
import dv.camt.project.projectkotlinmovie.repository.MoviesRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class MoviesDaoDBImpl:MoviesDao{
    override fun getMoviesById(movieId: Long): Movie {
        return moviesRepository.findMovieById(movieId)
    }

    override fun getMovies(): List<Movie> {
        return moviesRepository.findAll().filterIsInstance(Movie::class.java)
    }

    @Autowired
    lateinit var moviesRepository: MoviesRepository
}