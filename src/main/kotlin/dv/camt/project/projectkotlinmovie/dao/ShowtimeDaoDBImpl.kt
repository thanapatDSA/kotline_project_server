package dv.camt.project.projectkotlinmovie.dao

import dv.camt.project.projectkotlinmovie.entity.Showtime
import dv.camt.project.projectkotlinmovie.repository.ShowtimeRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class ShowtimeDaoDBImpl: ShowtimeDao{
    override fun getAllShowtimes(): List<Showtime> {
        return showtimeRepository.findAll().filterIsInstance(Showtime::class.java)
    }

    override fun getShowTimeById(id: Long): Showtime {
        return showtimeRepository.findShowtimeById(id)
    }

    override fun getShowTimeByMovieId(id: Long): List<Showtime> {
        return showtimeRepository.findByMovie_Id(id)
    }

    @Autowired
    lateinit var showtimeRepository: ShowtimeRepository
}