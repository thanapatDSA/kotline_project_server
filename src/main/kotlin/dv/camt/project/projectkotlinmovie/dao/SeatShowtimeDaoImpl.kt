package dv.camt.project.projectkotlinmovie.dao

import dv.camt.project.projectkotlinmovie.entity.SeatShowtime
import dv.camt.project.projectkotlinmovie.repository.SeatShowtimeRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class SeatShowtimeDaoImpl:SeatShowtimeDao {
    override fun save(seat: SeatShowtime): SeatShowtime {
        return seatShowtimeRepository.save(seat)
    }

    override fun findById(id: Long?): SeatShowtime {
        return seatShowtimeRepository.findById(id!!).orElse(null)
    }

    @Autowired
    lateinit var seatShowtimeRepository: SeatShowtimeRepository
}