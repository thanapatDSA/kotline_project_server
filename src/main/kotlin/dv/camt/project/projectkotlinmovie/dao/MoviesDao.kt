package dv.camt.project.projectkotlinmovie.dao

import dv.camt.project.projectkotlinmovie.entity.Movie
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository


interface MoviesDao {
    fun getMovies(): List<Movie>
    fun getMoviesById(movieId: Long): Movie
}