package dv.camt.project.projectkotlinmovie.dao

import dv.camt.project.projectkotlinmovie.entity.Cinema

interface CinemaDao {
    fun getAllCinema(): List<Cinema>

}