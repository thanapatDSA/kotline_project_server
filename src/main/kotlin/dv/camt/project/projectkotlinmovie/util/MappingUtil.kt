package dv.camt.project.projectkotlinmovie.util

import dv.camt.project.projectkotlinmovie.entity.*
import dv.camt.project.projectkotlinmovie.entity.dto.*
import org.mapstruct.InheritInverseConfiguration
import org.mapstruct.Mapper
import org.mapstruct.factory.Mappers

@Mapper(componentModel = "spring")
interface MapperUtil {
    companion object {
        val INSTANCE = Mappers.getMapper(MapperUtil::class.java)
    }

    fun mapMovies(movie: Movie?): MovieDto
    fun mapMovies(movies: List<Movie?>): List<MovieDto>
    fun mapShowTime(showtime: Showtime?): ShowTimeDto
    fun mapShowTime(showtime: List<Showtime?>): List<ShowTimeDto>
    fun mapShowTimeWithSeat(showtime: Showtime?): ShowTimeWithSeatDto
    fun mapShowTimeWithSeat(showtime: List<Showtime?>): List<ShowTimeWithSeatDto>
    fun mapShowTimeBooking(showtime: Showtime?): ShowTimeBookingDto
    fun mapShowTimeBooking(showtime: List<Showtime>?): List<ShowTimeBookingDto>
    fun mapCinema(cinema: Cinema?): CinemaDto
    fun mapCinema(cinema: List<Cinema?>): List<CinemaDto>
    fun mapSeat(seat: Seat?): SeatDto
    fun mapSeat(seat: List<Seat?>): List<SeatDto>

    fun mapUser(user: User): UserDto
    @InheritInverseConfiguration
    fun mapUser(user: UserDto): User

    fun mapUserRegister(user: User): UserRegiserDto
    @InheritInverseConfiguration
    fun mapUserRegister(user: UserRegiserDto): User

    fun mapUserUpdate(user: User): User

    fun mapBooking(booking: Booking):BookingDto
    fun mapBooking(booking: List<Booking>):List<BookingDto>
}
