package dv.camt.project.projectkotlinmovie.config


import dv.camt.project.projectkotlinmovie.entity.*
import dv.camt.project.projectkotlinmovie.repository.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import org.springframework.stereotype.Component
import javax.transaction.Transactional

@Component
class ApplicationLoader : ApplicationRunner {
    @Autowired
    lateinit var selectedSeatRepository: SelectedSeatRepository
    @Autowired
    lateinit var seatShowtimeRepository: SeatShowtimeRepository
    @Autowired
    lateinit var moviesRepository: MoviesRepository
    @Autowired
    lateinit var showtimeRepository: ShowtimeRepository
    @Autowired
    lateinit var seatRepository: SeatRepository
    @Autowired
    lateinit var cinemaRepository: CinemaRepository

    @Transactional
    override fun run(args: ApplicationArguments?) {
        val Day: Int = 86400000

        var premium = seatRepository.save(Seat("Premium", 190.00, 4, 20, SeatType.PREMIUM))
        var deluxe = seatRepository.save(Seat("Deluxe", 150.00, 10, 20, SeatType.DELUXE))
        var sofa = seatRepository.save(Seat("Sofa Sweet (Pair)", 500.00, 1, 6, SeatType.SOFA))

        var cinema1 = cinemaRepository.save(Cinema("Cinema 1"))
        var cinema2 = cinemaRepository.save(Cinema("Cinema 2"))
        var cinema3 = cinemaRepository.save(Cinema("Cinema 3"))

        cinema1.seat = mutableListOf(premium, deluxe, sofa)
        cinema2.seat = mutableListOf(premium, deluxe)
        cinema3.seat = mutableListOf(premium, deluxe, sofa)

        var movies1 = moviesRepository.save(Movie("อลิตา แบทเทิล แองเจิ้ล", 125, "https://lh3.googleusercontent.com/m-KIGZSYMapNnCIGB3QVXPlqkv7Vu_-VoEXqdleoNZ-CDRVdprMcoUanKbMHeFjlXk38FYPxW0Gar0XEQYI=w1024"))
        var showtime1_1 = showtimeRepository.save(Showtime(1553513400000, 1553513400000 + 7500000, Soundtrack.EN, Subtitles.TH))
        var showtime1_2 = showtimeRepository.save(Showtime(1553515200000, 1553515200000 + 7500000, Soundtrack.TH, Subtitles.TH))
        var showtime1_3 = showtimeRepository.save(Showtime(1553515200000 + Day, 1553515200000 + Day + 7500000, Soundtrack.EN, Subtitles.TH))
        var showtime1_4 = showtimeRepository.save(Showtime(1553533800000 + Day, 1553533800000 + Day + 7500000, Soundtrack.TH, Subtitles.TH))
        var showtime1_5 = showtimeRepository.save(Showtime(1553513400000 + Day * 2, 1553513400000 + Day * 2 + 7500000, Soundtrack.EN, Subtitles.TH))
        var showtime1_6 = showtimeRepository.save(Showtime(1553515200000 + Day * 2, 1553515200000 + Day * 2 + 7500000, Soundtrack.TH, Subtitles.TH))
        var showtime1_7 = showtimeRepository.save(Showtime(1553515200000 + Day * 3, 1553515200000 + Day * 3 + 7500000, Soundtrack.EN, Subtitles.TH))
        var showtime1_8 = showtimeRepository.save(Showtime(1553513400000 + Day * 3, 1553513400000 + Day * 3 + 7500000, Soundtrack.EN, Subtitles.TH))

        movies1.showtimes.add(showtime1_1)
        showtime1_1.movie = movies1
        showtime1_1.cinema = cinema1
        movies1.showtimes.add(showtime1_2)
        showtime1_2.movie = movies1
        showtime1_2.cinema = cinema2
        movies1.showtimes.add(showtime1_3)
        showtime1_3.movie = movies1
        showtime1_3.cinema = cinema1
        movies1.showtimes.add(showtime1_4)
        showtime1_4.movie = movies1
        showtime1_4.cinema = cinema3
        movies1.showtimes.add(showtime1_5)
        showtime1_5.movie = movies1
        showtime1_5.cinema = cinema1
        movies1.showtimes.add(showtime1_6)
        showtime1_6.movie = movies1
        showtime1_6.cinema = cinema2
        movies1.showtimes.add(showtime1_7)
        showtime1_7.movie = movies1
        showtime1_7.cinema = cinema1
        movies1.showtimes.add(showtime1_8)
        showtime1_8.movie = movies1
        showtime1_8.cinema = cinema2

        var movies2 = moviesRepository.save(Movie("อภินิหารไวกิ้งพิชิตมังกร 3", 105, "https://lh3.googleusercontent.com/xTpxPyQOfsnpA2lkp8Fz7LjVA-hsadGF0U7c5rwJq4ikRG_np-5_kO-gzHMVkur2Y5BnLQ4Sfki_Qj7T0Mlisw=w1024"))
        var showtime2_1 = showtimeRepository.save(Showtime(1553517000000, 1553517000000 + 6300000, Soundtrack.EN, Subtitles.TH))
        var showtime2_2 = showtimeRepository.save(Showtime(1553521200000, 1553521200000 + 6300000, Soundtrack.EN, Subtitles.TH))
        var showtime2_3 = showtimeRepository.save(Showtime(1553513400000 + Day, 1553513400000 + Day + 6300000, Soundtrack.EN, Subtitles.TH))
        var showtime2_4 = showtimeRepository.save(Showtime(1553517000000 + Day * 2, 1553517000000 + Day * 2 + 6300000, Soundtrack.EN, Subtitles.TH))
        var showtime2_5 = showtimeRepository.save(Showtime(1553517000000 + Day * 3, 1553517000000 + Day * 3 + 6300000, Soundtrack.EN, Subtitles.TH))
        var showtime2_6 = showtimeRepository.save(Showtime(1553547000000 + Day * 3, 1553547000000 + Day * 3 + 6300000, Soundtrack.EN, Subtitles.TH))

        movies2.showtimes.add(showtime2_1)
        showtime2_1.movie = movies2
        showtime2_1.cinema = cinema3
        movies2.showtimes.add(showtime2_2)
        showtime2_2.movie = movies2
        showtime2_2.cinema = cinema1
        movies2.showtimes.add(showtime2_3)
        showtime2_3.movie = movies2
        showtime2_3.cinema = cinema1
        movies2.showtimes.add(showtime2_4)
        showtime2_4.movie = movies2
        showtime2_4.cinema = cinema3
        movies2.showtimes.add(showtime2_5)
        showtime2_5.movie = movies2
        showtime2_5.cinema = cinema3
        movies2.showtimes.add(showtime2_6)
        showtime2_6.movie = movies2
        showtime2_6.cinema = cinema1

        var movies3 = moviesRepository.save(Movie("ปีกแห่งฝัน วันแห่งรัก", 120, "https://i.imgur.com/NErvf04.jpg"))
        var showtime3_1 = showtimeRepository.save(Showtime(1553525400000, 1553525400000 + 7200000, Soundtrack.JP, Subtitles.TH))
        var showtime3_2 = showtimeRepository.save(Showtime(1553533800000, 1553533800000 + 7200000, Soundtrack.JP, Subtitles.TH))
        var showtime3_3 = showtimeRepository.save(Showtime(1553517000000 + Day, 1553517000000 + Day + 7200000, Soundtrack.JP, Subtitles.TH))
        var showtime3_4 = showtimeRepository.save(Showtime(1553547600000 + Day, 1553547600000 + Day + 7200000, Soundtrack.JP, Subtitles.TH))
        var showtime3_5 = showtimeRepository.save(Showtime(1553533800000 + Day * 2, 1553533800000 + Day * 2 + 7200000, Soundtrack.JP, Subtitles.TH))
        var showtime3_6 = showtimeRepository.save(Showtime(1553521200000 + Day * 3, 1553521200000 + Day * 3 + 7200000, Soundtrack.JP, Subtitles.TH))

        movies3.showtimes.add(showtime3_1)
        showtime3_1.movie = movies3
        showtime3_1.cinema = cinema2
        movies3.showtimes.add(showtime3_2)
        showtime3_2.movie = movies3
        showtime3_2.cinema = cinema3
        movies3.showtimes.add(showtime3_3)
        showtime3_3.movie = movies3
        showtime3_3.cinema = cinema3
        movies3.showtimes.add(showtime3_4)
        showtime3_4.movie = movies3
        showtime3_4.cinema = cinema3
        movies3.showtimes.add(showtime3_5)
        showtime3_5.movie = movies3
        showtime3_5.cinema = cinema3
        movies3.showtimes.add(showtime3_6)
        showtime3_6.movie = movies3
        showtime3_6.cinema = cinema1

        var movies4 = moviesRepository.save(Movie("ยามซากุระร่วงโรย", 65, "https://i.imgur.com/8CtUUSj.jpg"))
        var showtime4_1 = showtimeRepository.save(Showtime(1553525400000, 1553525400000 + 3900000, Soundtrack.JP, Subtitles.TH))
        var showtime4_2 = showtimeRepository.save(Showtime(1553521200000 + Day, 1553521200000 + Day + 3900000, Soundtrack.JP, Subtitles.TH))
        var showtime4_3 = showtimeRepository.save(Showtime(1553521200000 + Day * 2, 1553521200000 + Day * 2 + 6300000, Soundtrack.EN, Subtitles.TH))
        var showtime4_4 = showtimeRepository.save(Showtime(1553525400000 + Day * 2, 1553525400000 + Day * 2 + 3900000, Soundtrack.JP, Subtitles.TH))
        var showtime4_5 = showtimeRepository.save(Showtime(1553525400000 + Day * 3, 1553525400000 + Day * 3 + 3900000, Soundtrack.JP, Subtitles.TH))
        var showtime4_6 = showtimeRepository.save(Showtime(1553544600000 + Day * 3, 1553544600000 + Day * 3 + 3900000, Soundtrack.JP, Subtitles.TH))

        movies4.showtimes.add(showtime4_1)
        showtime4_1.movie = movies4
        showtime4_1.cinema = cinema1
        movies4.showtimes.add(showtime4_2)
        showtime4_2.movie = movies4
        showtime4_2.cinema = cinema1
        movies4.showtimes.add(showtime4_3)
        showtime4_3.movie = movies4
        showtime4_3.cinema = cinema3
        movies4.showtimes.add(showtime4_4)
        showtime4_4.movie = movies4
        showtime4_4.cinema = cinema1
        movies4.showtimes.add(showtime4_5)
        showtime4_5.movie = movies4
        showtime4_5.cinema = cinema2
        movies4.showtimes.add(showtime4_6)
        showtime4_6.movie = movies4
        showtime4_6.cinema = cinema2


        var movies5 = moviesRepository.save(Movie("โมบิลสูทกันดั้ม ฮาธาเวยส์ แฟลช", 120, "https://i.imgur.com/OS3TPco.jpg"))
        var showtime5_1 = showtimeRepository.save(Showtime(1553547600000, 1553547600000 + 7200000, Soundtrack.JP, Subtitles.TH))
        var showtime5_2 = showtimeRepository.save(Showtime(1553525400000 + Day, 1553525400000 + Day + 7200000, Soundtrack.JP, Subtitles.TH))
        var showtime5_3 = showtimeRepository.save(Showtime(1553547000000 + Day, 1553547000000 + Day + 7200000, Soundtrack.JP, Subtitles.TH))
        var showtime5_4 = showtimeRepository.save(Showtime(1553544600000 + Day, 1553544600000 + Day + 7200000, Soundtrack.JP, Subtitles.TH))
        var showtime5_5 = showtimeRepository.save(Showtime(1553525400000 + Day * 2, 1553525400000 + Day * 2 + 7200000, Soundtrack.JP, Subtitles.TH))
        var showtime5_6 = showtimeRepository.save(Showtime(1553547600000 + Day * 2, 1553547600000 + Day * 2 + 7200000, Soundtrack.JP, Subtitles.TH))
        var showtime5_7 = showtimeRepository.save(Showtime(1553533800000 + Day * 3, 1553533800000 + Day * 3 + 7200000, Soundtrack.JP, Subtitles.TH))
        var showtime5_8 = showtimeRepository.save(Showtime(1553547600000 + Day * 3, 1553547600000 + Day * 3 + 7200000, Soundtrack.JP, Subtitles.TH))

        movies5.showtimes.add(showtime5_1)
        showtime5_1.movie = movies5
        showtime5_1.cinema = cinema2
        movies5.showtimes.add(showtime5_2)
        showtime5_2.movie = movies5
        showtime5_2.cinema = cinema2
        movies5.showtimes.add(showtime5_3)
        showtime5_3.movie = movies5
        showtime5_3.cinema = cinema1
        movies5.showtimes.add(showtime5_4)
        showtime5_4.movie = movies5
        showtime5_4.cinema = cinema2
        movies5.showtimes.add(showtime5_5)
        showtime5_5.movie = movies5
        showtime5_5.cinema = cinema2
        movies5.showtimes.add(showtime5_6)
        showtime5_6.movie = movies5
        showtime5_6.cinema = cinema2
        movies5.showtimes.add(showtime5_7)
        showtime5_7.movie = movies5
        showtime5_7.cinema = cinema3
        movies5.showtimes.add(showtime5_8)
        showtime5_8.movie = movies5
        showtime5_8.cinema = cinema3


/////////////////////////////showtime1_1/////////////////////////////////
        var row: Char = 'A'
        for ((index, item) in showtime1_1.cinema!!.seat.withIndex()) {
            var selectedSeat = selectedSeatRepository.save(SelectedSeat(item))
            for (indexR in 1..selectedSeat.seatDetail!!.row!!) {
                for (indexC in 1..selectedSeat.seatDetail!!.columns!!) {
                    var seat: SeatShowtime
                    if (item.type === SeatType.SOFA) {
                        seat = seatShowtimeRepository.save(SeatShowtime(false, "${row}${row}", "C${indexC}"))
                    } else {
                        seat = seatShowtimeRepository.save(SeatShowtime(false, "${row}", "C${indexC}"))
                    }
                    selectedSeat.seats.add(seat)
                }
                row++
            }
            showtime1_1.seats.add(selectedSeat)
            if (item.type === SeatType.SOFA) {
                row = 'A'
            }
        }
/////////////////////////////showtime1_2/////////////////////////////////
        row = 'A'
        for ((index, item) in showtime1_2.cinema!!.seat.withIndex()) {
            var selectedSeat = selectedSeatRepository.save(SelectedSeat(item))
            for (indexR in 1..selectedSeat.seatDetail!!.row!!) {
                for (indexC in 1..selectedSeat.seatDetail!!.columns!!) {
                    var seat: SeatShowtime
                    if (item.type === SeatType.SOFA) {
                        seat = seatShowtimeRepository.save(SeatShowtime(false, "${row}${row}", "C${indexC}"))
                    } else {
                        seat = seatShowtimeRepository.save(SeatShowtime(false, "${row}", "C${indexC}"))
                    }
                    selectedSeat.seats.add(seat)
                }
                row++
            }
            showtime1_2.seats.add(selectedSeat)
            if (item.type === SeatType.SOFA) {
                row = 'A'
            }
        }

        /////////////////////////////showtime1_3/////////////////////////////////
        row = 'A'
        for ((index, item) in showtime1_3.cinema!!.seat.withIndex()) {
            var selectedSeat = selectedSeatRepository.save(SelectedSeat(item))
            for (indexR in 1..selectedSeat.seatDetail!!.row!!) {
                for (indexC in 1..selectedSeat.seatDetail!!.columns!!) {
                    var seat: SeatShowtime
                    if (item.type === SeatType.SOFA) {
                        seat = seatShowtimeRepository.save(SeatShowtime(false, "${row}${row}", "C${indexC}"))
                    } else {
                        seat = seatShowtimeRepository.save(SeatShowtime(false, "${row}", "C${indexC}"))
                    }
                    selectedSeat.seats.add(seat)
                }
                row++
            }
            showtime1_3.seats.add(selectedSeat)
            if (item.type === SeatType.SOFA) {
                row = 'A'
            }
        }
        /////////////////////////////showtime1_4/////////////////////////////////
        row = 'A'
        for ((index, item) in showtime1_4.cinema!!.seat.withIndex()) {
            var selectedSeat = selectedSeatRepository.save(SelectedSeat(item))
            for (indexR in 1..selectedSeat.seatDetail!!.row!!) {
                for (indexC in 1..selectedSeat.seatDetail!!.columns!!) {
                    var seat: SeatShowtime
                    if (item.type === SeatType.SOFA) {
                        seat = seatShowtimeRepository.save(SeatShowtime(false, "${row}${row}", "C${indexC}"))
                    } else {
                        seat = seatShowtimeRepository.save(SeatShowtime(false, "${row}", "C${indexC}"))
                    }
                    selectedSeat.seats.add(seat)
                }
                row++
            }
            showtime1_4.seats.add(selectedSeat)
            if (item.type === SeatType.SOFA) {
                row = 'A'
            }
        }
        /////////////////////////////showtime1_5/////////////////////////////////
        row = 'A'
        for ((index, item) in showtime1_5.cinema!!.seat.withIndex()) {
            var selectedSeat = selectedSeatRepository.save(SelectedSeat(item))
            for (indexR in 1..selectedSeat.seatDetail!!.row!!) {
                for (indexC in 1..selectedSeat.seatDetail!!.columns!!) {
                    var seat: SeatShowtime
                    if (item.type === SeatType.SOFA) {
                        seat = seatShowtimeRepository.save(SeatShowtime(false, "${row}${row}", "C${indexC}"))
                    } else {
                        seat = seatShowtimeRepository.save(SeatShowtime(false, "${row}", "C${indexC}"))
                    }
                    selectedSeat.seats.add(seat)
                }
                row++
            }
            showtime1_5.seats.add(selectedSeat)
            if (item.type === SeatType.SOFA) {
                row = 'A'
            }
        }
        /////////////////////////////showtime1_6/////////////////////////////////
        row = 'A'
        for ((index, item) in showtime1_6.cinema!!.seat.withIndex()) {
            var selectedSeat = selectedSeatRepository.save(SelectedSeat(item))
            for (indexR in 1..selectedSeat.seatDetail!!.row!!) {
                for (indexC in 1..selectedSeat.seatDetail!!.columns!!) {
                    var seat: SeatShowtime
                    if (item.type === SeatType.SOFA) {
                        seat = seatShowtimeRepository.save(SeatShowtime(false, "${row}${row}", "C${indexC}"))
                    } else {
                        seat = seatShowtimeRepository.save(SeatShowtime(false, "${row}", "C${indexC}"))
                    }
                    selectedSeat.seats.add(seat)
                }
                row++
            }
            showtime1_6.seats.add(selectedSeat)
            if (item.type === SeatType.SOFA) {
                row = 'A'
            }
        }
        /////////////////////////////showtime1_7/////////////////////////////////
        row = 'A'
        for ((index, item) in showtime1_7.cinema!!.seat.withIndex()) {
            var selectedSeat = selectedSeatRepository.save(SelectedSeat(item))
            for (indexR in 1..selectedSeat.seatDetail!!.row!!) {
                for (indexC in 1..selectedSeat.seatDetail!!.columns!!) {
                    var seat: SeatShowtime
                    if (item.type === SeatType.SOFA) {
                        seat = seatShowtimeRepository.save(SeatShowtime(false, "${row}${row}", "C${indexC}"))
                    } else {
                        seat = seatShowtimeRepository.save(SeatShowtime(false, "${row}", "C${indexC}"))
                    }
                    selectedSeat.seats.add(seat)
                }
                row++
            }
            showtime1_7.seats.add(selectedSeat)
            if (item.type === SeatType.SOFA) {
                row = 'A'
            }
        }
        /////////////////////////////showtime1_8/////////////////////////////////
        row = 'A'
        for ((index, item) in showtime1_8.cinema!!.seat.withIndex()) {
            var selectedSeat = selectedSeatRepository.save(SelectedSeat(item))
            for (indexR in 1..selectedSeat.seatDetail!!.row!!) {
                for (indexC in 1..selectedSeat.seatDetail!!.columns!!) {
                    var seat: SeatShowtime
                    if (item.type === SeatType.SOFA) {
                        seat = seatShowtimeRepository.save(SeatShowtime(false, "${row}${row}", "C${indexC}"))
                    } else {
                        seat = seatShowtimeRepository.save(SeatShowtime(false, "${row}", "C${indexC}"))
                    }
                    selectedSeat.seats.add(seat)
                }
                row++
            }
            showtime1_8.seats.add(selectedSeat)
            if (item.type === SeatType.SOFA) {
                row = 'A'
            }
        }

        /////////////////////////////showtime2_1/////////////////////////////////
        row = 'A'
        for ((index, item) in showtime2_1.cinema!!.seat.withIndex()) {
            var selectedSeat = selectedSeatRepository.save(SelectedSeat(item))
            for (indexR in 1..selectedSeat.seatDetail!!.row!!) {
                for (indexC in 1..selectedSeat.seatDetail!!.columns!!) {
                    var seat: SeatShowtime
                    if (item.type === SeatType.SOFA) {
                        seat = seatShowtimeRepository.save(SeatShowtime(false, "${row}${row}", "C${indexC}"))
                    } else {
                        seat = seatShowtimeRepository.save(SeatShowtime(false, "${row}", "C${indexC}"))
                    }
                    selectedSeat.seats.add(seat)
                }
                row++
            }
            showtime2_1.seats.add(selectedSeat)
            if (item.type === SeatType.SOFA) {
                row = 'A'
            }
        }
        /////////////////////////////showtime2_2/////////////////////////////////
        row = 'A'
        for ((index, item) in showtime2_2.cinema!!.seat.withIndex()) {
            var selectedSeat = selectedSeatRepository.save(SelectedSeat(item))
            for (indexR in 1..selectedSeat.seatDetail!!.row!!) {
                for (indexC in 1..selectedSeat.seatDetail!!.columns!!) {
                    var seat: SeatShowtime
                    if (item.type === SeatType.SOFA) {
                        seat = seatShowtimeRepository.save(SeatShowtime(false, "${row}${row}", "C${indexC}"))
                    } else {
                        seat = seatShowtimeRepository.save(SeatShowtime(false, "${row}", "C${indexC}"))
                    }
                    selectedSeat.seats.add(seat)
                }
                row++
            }
            showtime2_2.seats.add(selectedSeat)
            if (item.type === SeatType.SOFA) {
                row = 'A'
            }
        }
        /////////////////////////////showtime2_3/////////////////////////////////
        row = 'A'
        for ((index, item) in showtime2_3.cinema!!.seat.withIndex()) {
            var selectedSeat = selectedSeatRepository.save(SelectedSeat(item))
            for (indexR in 1..selectedSeat.seatDetail!!.row!!) {
                for (indexC in 1..selectedSeat.seatDetail!!.columns!!) {
                    var seat: SeatShowtime
                    if (item.type === SeatType.SOFA) {
                        seat = seatShowtimeRepository.save(SeatShowtime(false, "${row}${row}", "C${indexC}"))
                    } else {
                        seat = seatShowtimeRepository.save(SeatShowtime(false, "${row}", "C${indexC}"))
                    }
                    selectedSeat.seats.add(seat)
                }
                row++
            }
            showtime2_3.seats.add(selectedSeat)
            if (item.type === SeatType.SOFA) {
                row = 'A'
            }
        }
        /////////////////////////////showtime2_4/////////////////////////////////
        row = 'A'
        for ((index, item) in showtime2_4.cinema!!.seat.withIndex()) {
            var selectedSeat = selectedSeatRepository.save(SelectedSeat(item))
            for (indexR in 1..selectedSeat.seatDetail!!.row!!) {
                for (indexC in 1..selectedSeat.seatDetail!!.columns!!) {
                    var seat: SeatShowtime
                    if (item.type === SeatType.SOFA) {
                        seat = seatShowtimeRepository.save(SeatShowtime(false, "${row}${row}", "C${indexC}"))
                    } else {
                        seat = seatShowtimeRepository.save(SeatShowtime(false, "${row}", "C${indexC}"))
                    }
                    selectedSeat.seats.add(seat)
                }
                row++
            }
            showtime2_4.seats.add(selectedSeat)
            if (item.type === SeatType.SOFA) {
                row = 'A'
            }
        }
        /////////////////////////////showtime2_5/////////////////////////////////
        row = 'A'
        for ((index, item) in showtime2_5.cinema!!.seat.withIndex()) {
            var selectedSeat = selectedSeatRepository.save(SelectedSeat(item))
            for (indexR in 1..selectedSeat.seatDetail!!.row!!) {
                for (indexC in 1..selectedSeat.seatDetail!!.columns!!) {
                    var seat: SeatShowtime
                    if (item.type === SeatType.SOFA) {
                        seat = seatShowtimeRepository.save(SeatShowtime(false, "${row}${row}", "C${indexC}"))
                    } else {
                        seat = seatShowtimeRepository.save(SeatShowtime(false, "${row}", "C${indexC}"))
                    }
                    selectedSeat.seats.add(seat)
                }
                row++
            }
            showtime2_5.seats.add(selectedSeat)
            if (item.type === SeatType.SOFA) {
                row = 'A'
            }
        }
        /////////////////////////////showtime2_6/////////////////////////////////
        row = 'A'
        for ((index, item) in showtime2_6.cinema!!.seat.withIndex()) {
            var selectedSeat = selectedSeatRepository.save(SelectedSeat(item))
            for (indexR in 1..selectedSeat.seatDetail!!.row!!) {
                for (indexC in 1..selectedSeat.seatDetail!!.columns!!) {
                    var seat: SeatShowtime
                    if (item.type === SeatType.SOFA) {
                        seat = seatShowtimeRepository.save(SeatShowtime(false, "${row}${row}", "C${indexC}"))
                    } else {
                        seat = seatShowtimeRepository.save(SeatShowtime(false, "${row}", "C${indexC}"))
                    }
                    selectedSeat.seats.add(seat)
                }
                row++
            }
            showtime2_6.seats.add(selectedSeat)
            if (item.type === SeatType.SOFA) {
                row = 'A'
            }
        }
        /////////////////////////////showtime3_1/////////////////////////////////
        row = 'A'
        for ((index, item) in showtime3_1.cinema!!.seat.withIndex()) {
            var selectedSeat = selectedSeatRepository.save(SelectedSeat(item))
            for (indexR in 1..selectedSeat.seatDetail!!.row!!) {
                for (indexC in 1..selectedSeat.seatDetail!!.columns!!) {
                    var seat: SeatShowtime
                    if (item.type === SeatType.SOFA) {
                        seat = seatShowtimeRepository.save(SeatShowtime(false, "${row}${row}", "C${indexC}"))
                    } else {
                        seat = seatShowtimeRepository.save(SeatShowtime(false, "${row}", "C${indexC}"))
                    }
                    selectedSeat.seats.add(seat)
                }
                row++
            }
            showtime3_1.seats.add(selectedSeat)
            if (item.type === SeatType.SOFA) {
                row = 'A'
            }
        }
        /////////////////////////////showtime3_2/////////////////////////////////
        row = 'A'
        for ((index, item) in showtime3_2.cinema!!.seat.withIndex()) {
            var selectedSeat = selectedSeatRepository.save(SelectedSeat(item))
            for (indexR in 1..selectedSeat.seatDetail!!.row!!) {
                for (indexC in 1..selectedSeat.seatDetail!!.columns!!) {
                    var seat: SeatShowtime
                    if (item.type === SeatType.SOFA) {
                        seat = seatShowtimeRepository.save(SeatShowtime(false, "${row}${row}", "C${indexC}"))
                    } else {
                        seat = seatShowtimeRepository.save(SeatShowtime(false, "${row}", "C${indexC}"))
                    }
                    selectedSeat.seats.add(seat)
                }
                row++
            }
            showtime3_2.seats.add(selectedSeat)
            if (item.type === SeatType.SOFA) {
                row = 'A'
            }
        }
        /////////////////////////////showtime3_3/////////////////////////////////
        row = 'A'
        for ((index, item) in showtime3_3.cinema!!.seat.withIndex()) {
            var selectedSeat = selectedSeatRepository.save(SelectedSeat(item))
            for (indexR in 1..selectedSeat.seatDetail!!.row!!) {
                for (indexC in 1..selectedSeat.seatDetail!!.columns!!) {
                    var seat: SeatShowtime
                    if (item.type === SeatType.SOFA) {
                        seat = seatShowtimeRepository.save(SeatShowtime(false, "${row}${row}", "C${indexC}"))
                    } else {
                        seat = seatShowtimeRepository.save(SeatShowtime(false, "${row}", "C${indexC}"))
                    }
                    selectedSeat.seats.add(seat)
                }
                row++
            }
            showtime3_3.seats.add(selectedSeat)
            if (item.type === SeatType.SOFA) {
                row = 'A'
            }
        }
        /////////////////////////////showtime3_4/////////////////////////////////
        row = 'A'
        for ((index, item) in showtime3_4.cinema!!.seat.withIndex()) {
            var selectedSeat = selectedSeatRepository.save(SelectedSeat(item))
            for (indexR in 1..selectedSeat.seatDetail!!.row!!) {
                for (indexC in 1..selectedSeat.seatDetail!!.columns!!) {
                    var seat: SeatShowtime
                    if (item.type === SeatType.SOFA) {
                        seat = seatShowtimeRepository.save(SeatShowtime(false, "${row}${row}", "C${indexC}"))
                    } else {
                        seat = seatShowtimeRepository.save(SeatShowtime(false, "${row}", "C${indexC}"))
                    }
                    selectedSeat.seats.add(seat)
                }
                row++
            }
            showtime3_4.seats.add(selectedSeat)
            if (item.type === SeatType.SOFA) {
                row = 'A'
            }
        }
        /////////////////////////////showtime3_5/////////////////////////////////
        row = 'A'
        for ((index, item) in showtime3_5.cinema!!.seat.withIndex()) {
            var selectedSeat = selectedSeatRepository.save(SelectedSeat(item))
            for (indexR in 1..selectedSeat.seatDetail!!.row!!) {
                for (indexC in 1..selectedSeat.seatDetail!!.columns!!) {
                    var seat: SeatShowtime
                    if (item.type === SeatType.SOFA) {
                        seat = seatShowtimeRepository.save(SeatShowtime(false, "${row}${row}", "C${indexC}"))
                    } else {
                        seat = seatShowtimeRepository.save(SeatShowtime(false, "${row}", "C${indexC}"))
                    }
                    selectedSeat.seats.add(seat)
                }
                row++
            }
            showtime3_5.seats.add(selectedSeat)
            if (item.type === SeatType.SOFA) {
                row = 'A'
            }
        }
        /////////////////////////////showtime3_6/////////////////////////////////
        row = 'A'
        for ((index, item) in showtime3_6.cinema!!.seat.withIndex()) {
            var selectedSeat = selectedSeatRepository.save(SelectedSeat(item))
            for (indexR in 1..selectedSeat.seatDetail!!.row!!) {
                for (indexC in 1..selectedSeat.seatDetail!!.columns!!) {
                    var seat: SeatShowtime
                    if (item.type === SeatType.SOFA) {
                        seat = seatShowtimeRepository.save(SeatShowtime(false, "${row}${row}", "C${indexC}"))
                    } else {
                        seat = seatShowtimeRepository.save(SeatShowtime(false, "${row}", "C${indexC}"))
                    }
                    selectedSeat.seats.add(seat)
                }
                row++
            }
            showtime3_6.seats.add(selectedSeat)
            if (item.type === SeatType.SOFA) {
                row = 'A'
            }
        }

        /////////////////////////////showtime4_1/////////////////////////////////
        row = 'A'
        for ((index, item) in showtime4_1.cinema!!.seat.withIndex()) {
            var selectedSeat = selectedSeatRepository.save(SelectedSeat(item))
            for (indexR in 1..selectedSeat.seatDetail!!.row!!) {
                for (indexC in 1..selectedSeat.seatDetail!!.columns!!) {
                    var seat: SeatShowtime
                    if (item.type === SeatType.SOFA) {
                        seat = seatShowtimeRepository.save(SeatShowtime(false, "${row}${row}", "C${indexC}"))
                    } else {
                        seat = seatShowtimeRepository.save(SeatShowtime(false, "${row}", "C${indexC}"))
                    }
                    selectedSeat.seats.add(seat)
                }
                row++
            }
            showtime4_1.seats.add(selectedSeat)
            if (item.type === SeatType.SOFA) {
                row = 'A'
            }
        }
        /////////////////////////////showtime4_2/////////////////////////////////
        row = 'A'
        for ((index, item) in showtime4_2.cinema!!.seat.withIndex()) {
            var selectedSeat = selectedSeatRepository.save(SelectedSeat(item))
            for (indexR in 1..selectedSeat.seatDetail!!.row!!) {
                for (indexC in 1..selectedSeat.seatDetail!!.columns!!) {
                    var seat: SeatShowtime
                    if (item.type === SeatType.SOFA) {
                        seat = seatShowtimeRepository.save(SeatShowtime(false, "${row}${row}", "C${indexC}"))
                    } else {
                        seat = seatShowtimeRepository.save(SeatShowtime(false, "${row}", "C${indexC}"))
                    }
                    selectedSeat.seats.add(seat)
                }
                row++
            }
            showtime4_2.seats.add(selectedSeat)
            if (item.type === SeatType.SOFA) {
                row = 'A'
            }
        }
        /////////////////////////////showtime4_3/////////////////////////////////
        row = 'A'
        for ((index, item) in showtime4_3.cinema!!.seat.withIndex()) {
            var selectedSeat = selectedSeatRepository.save(SelectedSeat(item))
            for (indexR in 1..selectedSeat.seatDetail!!.row!!) {
                for (indexC in 1..selectedSeat.seatDetail!!.columns!!) {
                    var seat: SeatShowtime
                    if (item.type === SeatType.SOFA) {
                        seat = seatShowtimeRepository.save(SeatShowtime(false, "${row}${row}", "C${indexC}"))
                    } else {
                        seat = seatShowtimeRepository.save(SeatShowtime(false, "${row}", "C${indexC}"))
                    }
                    selectedSeat.seats.add(seat)
                }
                row++
            }
            showtime4_3.seats.add(selectedSeat)
            if (item.type === SeatType.SOFA) {
                row = 'A'
            }
        }
        /////////////////////////////showtime4_4/////////////////////////////////
        row = 'A'
        for ((index, item) in showtime4_4.cinema!!.seat.withIndex()) {
            var selectedSeat = selectedSeatRepository.save(SelectedSeat(item))
            for (indexR in 1..selectedSeat.seatDetail!!.row!!) {
                for (indexC in 1..selectedSeat.seatDetail!!.columns!!) {
                    var seat: SeatShowtime
                    if (item.type === SeatType.SOFA) {
                        seat = seatShowtimeRepository.save(SeatShowtime(false, "${row}${row}", "C${indexC}"))
                    } else {
                        seat = seatShowtimeRepository.save(SeatShowtime(false, "${row}", "C${indexC}"))
                    }
                    selectedSeat.seats.add(seat)
                }
                row++
            }
            showtime4_4.seats.add(selectedSeat)
            if (item.type === SeatType.SOFA) {
                row = 'A'
            }
        }
        /////////////////////////////showtime4_5/////////////////////////////////
        row = 'A'
        for ((index, item) in showtime4_5.cinema!!.seat.withIndex()) {
            var selectedSeat = selectedSeatRepository.save(SelectedSeat(item))
            for (indexR in 1..selectedSeat.seatDetail!!.row!!) {
                for (indexC in 1..selectedSeat.seatDetail!!.columns!!) {
                    var seat: SeatShowtime
                    if (item.type === SeatType.SOFA) {
                        seat = seatShowtimeRepository.save(SeatShowtime(false, "${row}${row}", "C${indexC}"))
                    } else {
                        seat = seatShowtimeRepository.save(SeatShowtime(false, "${row}", "C${indexC}"))
                    }
                    selectedSeat.seats.add(seat)
                }
                row++
            }
            showtime4_5.seats.add(selectedSeat)
            if (item.type === SeatType.SOFA) {
                row = 'A'
            }
        }
        /////////////////////////////showtime4_6/////////////////////////////////
        row = 'A'
        for ((index, item) in showtime4_6.cinema!!.seat.withIndex()) {
            var selectedSeat = selectedSeatRepository.save(SelectedSeat(item))
            for (indexR in 1..selectedSeat.seatDetail!!.row!!) {
                for (indexC in 1..selectedSeat.seatDetail!!.columns!!) {
                    var seat: SeatShowtime
                    if (item.type === SeatType.SOFA) {
                        seat = seatShowtimeRepository.save(SeatShowtime(false, "${row}${row}", "C${indexC}"))
                    } else {
                        seat = seatShowtimeRepository.save(SeatShowtime(false, "${row}", "C${indexC}"))
                    }
                    selectedSeat.seats.add(seat)
                }
                row++
            }
            showtime4_6.seats.add(selectedSeat)
            if (item.type === SeatType.SOFA) {
                row = 'A'
            }
        }
        /////////////////////////////showtime5_1/////////////////////////////////
        row = 'A'
        for ((index, item) in showtime5_1.cinema!!.seat.withIndex()) {
            var selectedSeat = selectedSeatRepository.save(SelectedSeat(item))
            for (indexR in 1..selectedSeat.seatDetail!!.row!!) {
                for (indexC in 1..selectedSeat.seatDetail!!.columns!!) {
                    var seat: SeatShowtime
                    if (item.type === SeatType.SOFA) {
                        seat = seatShowtimeRepository.save(SeatShowtime(false, "${row}${row}", "C${indexC}"))
                    } else {
                        seat = seatShowtimeRepository.save(SeatShowtime(false, "${row}", "C${indexC}"))
                    }
                    selectedSeat.seats.add(seat)
                }
                row++
            }
            showtime5_1.seats.add(selectedSeat)
            if (item.type === SeatType.SOFA) {
                row = 'A'
            }
        }
        /////////////////////////////showtime5_2/////////////////////////////////
        row = 'A'
        for ((index, item) in showtime5_2.cinema!!.seat.withIndex()) {
            var selectedSeat = selectedSeatRepository.save(SelectedSeat(item))
            for (indexR in 1..selectedSeat.seatDetail!!.row!!) {
                for (indexC in 1..selectedSeat.seatDetail!!.columns!!) {
                    var seat: SeatShowtime
                    if (item.type === SeatType.SOFA) {
                        seat = seatShowtimeRepository.save(SeatShowtime(false, "${row}${row}", "C${indexC}"))
                    } else {
                        seat = seatShowtimeRepository.save(SeatShowtime(false, "${row}", "C${indexC}"))
                    }
                    selectedSeat.seats.add(seat)
                }
                row++
            }
            showtime5_2.seats.add(selectedSeat)
            if (item.type === SeatType.SOFA) {
                row = 'A'
            }
        }

        /////////////////////////////showtime5_3/////////////////////////////////
        row = 'A'
        for ((index, item) in showtime5_3.cinema!!.seat.withIndex()) {
            var selectedSeat = selectedSeatRepository.save(SelectedSeat(item))
            for (indexR in 1..selectedSeat.seatDetail!!.row!!) {
                for (indexC in 1..selectedSeat.seatDetail!!.columns!!) {
                    var seat: SeatShowtime
                    if (item.type === SeatType.SOFA) {
                        seat = seatShowtimeRepository.save(SeatShowtime(false, "${row}${row}", "C${indexC}"))
                    } else {
                        seat = seatShowtimeRepository.save(SeatShowtime(false, "${row}", "C${indexC}"))
                    }
                    selectedSeat.seats.add(seat)
                }
                row++
            }
            showtime5_3.seats.add(selectedSeat)
            if (item.type === SeatType.SOFA) {
                row = 'A'
            }
        }
        /////////////////////////////showtime5_4/////////////////////////////////
        row = 'A'
        for ((index, item) in showtime5_4.cinema!!.seat.withIndex()) {
            var selectedSeat = selectedSeatRepository.save(SelectedSeat(item))
            for (indexR in 1..selectedSeat.seatDetail!!.row!!) {
                for (indexC in 1..selectedSeat.seatDetail!!.columns!!) {
                    var seat: SeatShowtime
                    if (item.type === SeatType.SOFA) {
                        seat = seatShowtimeRepository.save(SeatShowtime(false, "${row}${row}", "C${indexC}"))
                    } else {
                        seat = seatShowtimeRepository.save(SeatShowtime(false, "${row}", "C${indexC}"))
                    }
                    selectedSeat.seats.add(seat)
                }
                row++
            }
            showtime5_4.seats.add(selectedSeat)
            if (item.type === SeatType.SOFA) {
                row = 'A'
            }
        }
        /////////////////////////////showtime5_5/////////////////////////////////
        row = 'A'
        for ((index, item) in showtime5_5.cinema!!.seat.withIndex()) {
            var selectedSeat = selectedSeatRepository.save(SelectedSeat(item))
            for (indexR in 1..selectedSeat.seatDetail!!.row!!) {
                for (indexC in 1..selectedSeat.seatDetail!!.columns!!) {
                    var seat: SeatShowtime
                    if (item.type === SeatType.SOFA) {
                        seat = seatShowtimeRepository.save(SeatShowtime(false, "${row}${row}", "C${indexC}"))
                    } else {
                        seat = seatShowtimeRepository.save(SeatShowtime(false, "${row}", "C${indexC}"))
                    }
                    selectedSeat.seats.add(seat)
                }
                row++
            }
            showtime5_5.seats.add(selectedSeat)
            if (item.type === SeatType.SOFA) {
                row = 'A'
            }
        }
        /////////////////////////////showtime5_6/////////////////////////////////
        row = 'A'
        for ((index, item) in showtime5_6.cinema!!.seat.withIndex()) {
            var selectedSeat = selectedSeatRepository.save(SelectedSeat(item))
            for (indexR in 1..selectedSeat.seatDetail!!.row!!) {
                for (indexC in 1..selectedSeat.seatDetail!!.columns!!) {
                    var seat: SeatShowtime
                    if (item.type === SeatType.SOFA) {
                        seat = seatShowtimeRepository.save(SeatShowtime(false, "${row}${row}", "C${indexC}"))
                    } else {
                        seat = seatShowtimeRepository.save(SeatShowtime(false, "${row}", "C${indexC}"))
                    }
                    selectedSeat.seats.add(seat)
                }
                row++
            }
            showtime5_6.seats.add(selectedSeat)
            if (item.type === SeatType.SOFA) {
                row = 'A'
            }
        }
        /////////////////////////////showtime5_7/////////////////////////////////
        row = 'A'
        for ((index, item) in showtime5_7.cinema!!.seat.withIndex()) {
            var selectedSeat = selectedSeatRepository.save(SelectedSeat(item))
            for (indexR in 1..selectedSeat.seatDetail!!.row!!) {
                for (indexC in 1..selectedSeat.seatDetail!!.columns!!) {
                    var seat: SeatShowtime
                    if (item.type === SeatType.SOFA) {
                        seat = seatShowtimeRepository.save(SeatShowtime(false, "${row}${row}", "C${indexC}"))
                    } else {
                        seat = seatShowtimeRepository.save(SeatShowtime(false, "${row}", "C${indexC}"))
                    }
                    selectedSeat.seats.add(seat)
                }
                row++
            }
            showtime5_7.seats.add(selectedSeat)
            if (item.type === SeatType.SOFA) {
                row = 'A'
            }
        }
        /////////////////////////////showtime5_8/////////////////////////////////
        row = 'A'
        for ((index, item) in showtime5_8.cinema!!.seat.withIndex()) {
            var selectedSeat = selectedSeatRepository.save(SelectedSeat(item))
            for (indexR in 1..selectedSeat.seatDetail!!.row!!) {
                for (indexC in 1..selectedSeat.seatDetail!!.columns!!) {
                    var seat: SeatShowtime
                    if (item.type === SeatType.SOFA) {
                        seat = seatShowtimeRepository.save(SeatShowtime(false, "${row}${row}", "C${indexC}"))
                    } else {
                        seat = seatShowtimeRepository.save(SeatShowtime(false, "${row}", "C${indexC}"))
                    }
                    selectedSeat.seats.add(seat)
                }
                row++
            }
            showtime5_8.seats.add(selectedSeat)
            if (item.type === SeatType.SOFA) {
                row = 'A'
            }
        }

    }
}