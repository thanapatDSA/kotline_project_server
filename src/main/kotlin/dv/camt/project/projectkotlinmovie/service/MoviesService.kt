package dv.camt.project.projectkotlinmovie.service

import dv.camt.project.projectkotlinmovie.entity.Movie

interface MoviesService{
    fun getMovies(): List<Movie>
    fun getMoviesById(movieId: Long): Movie
}