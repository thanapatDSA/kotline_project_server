package dv.camt.project.projectkotlinmovie.service

import dv.camt.project.projectkotlinmovie.dao.ShowtimeDao
import dv.camt.project.projectkotlinmovie.entity.Showtime
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class ShowTimeServiceImpl:ShowTimeService{
    override fun getAllShowtime(): List<Showtime> {
        return showtimeDao.getAllShowtimes()
    }

    override fun getShowTimeById(id: Long): Showtime {
        return showtimeDao.getShowTimeById(id)
    }

    @Autowired
    lateinit var showtimeDao: ShowtimeDao
    override fun getShowTimeByMovieId(id: Long): List<Showtime> {
        return showtimeDao.getShowTimeByMovieId(id)
    }

}