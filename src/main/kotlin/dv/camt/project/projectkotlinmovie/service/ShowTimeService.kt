package dv.camt.project.projectkotlinmovie.service

import dv.camt.project.projectkotlinmovie.entity.Showtime


interface ShowTimeService{
    fun getShowTimeByMovieId(id: Long): List<Showtime>
    fun getShowTimeById(id: Long): Showtime
    fun getAllShowtime(): List<Showtime>

}