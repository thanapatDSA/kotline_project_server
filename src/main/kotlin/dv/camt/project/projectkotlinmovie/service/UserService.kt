package dv.camt.project.projectkotlinmovie.service

import dv.camt.project.projectkotlinmovie.entity.User
import dv.camt.project.projectkotlinmovie.entity.dto.UserDto
import dv.camt.project.projectkotlinmovie.entity.dto.UserLoginDto
import dv.camt.project.projectkotlinmovie.entity.dto.UserPasswordDto
import dv.camt.project.projectkotlinmovie.entity.dto.UserRegiserDto

interface UserService{
    fun save(user: UserRegiserDto): User
    fun edit(user: UserDto): User
    fun chagepassword(user: UserPasswordDto): User
    fun login(user: UserLoginDto): User
    fun findUserById(id: Long): User
}