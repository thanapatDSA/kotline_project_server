package dv.camt.project.projectkotlinmovie.service

import dv.camt.project.projectkotlinmovie.dao.CinemaDao
import dv.camt.project.projectkotlinmovie.entity.Cinema
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class CinemaServiceImpl: CinemaService{
    override fun getAllCinema(): List<Cinema> {
        return cinemaDao.getAllCinema()
    }

    @Autowired
    lateinit var cinemaDao: CinemaDao
}