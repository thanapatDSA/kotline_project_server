package dv.camt.project.projectkotlinmovie.service

import dv.camt.project.projectkotlinmovie.dao.BookingDao
import dv.camt.project.projectkotlinmovie.dao.SeatShowtimeDao
import dv.camt.project.projectkotlinmovie.dao.ShowtimeDao
import dv.camt.project.projectkotlinmovie.dao.UserDao
import dv.camt.project.projectkotlinmovie.entity.Booking
import dv.camt.project.projectkotlinmovie.entity.SeatShowtime
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class BookingServiceImpl : BookingService {
    override fun getBooking(userId: Long): List<Booking> {
        var user = userDao.findUserById(userId)
        return bookingDao.getBooking(user)
    }

    override fun booking(booking: Booking, showtimeId: Long, userId: Long): Booking {
        var showTime = showtimeDao.getShowTimeById(showtimeId)
        var seatMap = mutableListOf<SeatShowtime>()
        for (item in booking.seats) {
            var seat = seatShowtimeDao.findById(item.id)
            seat.status = true
            seatMap.add(seatShowtimeDao.save(seat))
        }
        var book = Booking()
        book.showtime = showTime
        book.seats = seatMap
        book.creatDateTime = System.currentTimeMillis()
        book.user = userDao.findUserById(userId)
        return bookingDao.save(book)
    }
    @Autowired
    lateinit var userDao: UserDao

    @Autowired
    lateinit var bookingDao: BookingDao

    @Autowired
    lateinit var seatShowtimeDao: SeatShowtimeDao

    @Autowired
    lateinit var showtimeDao: ShowtimeDao
}