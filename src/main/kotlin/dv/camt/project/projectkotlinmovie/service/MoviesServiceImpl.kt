package dv.camt.project.projectkotlinmovie.service

import dv.camt.project.projectkotlinmovie.dao.MoviesDao
import dv.camt.project.projectkotlinmovie.entity.Movie
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class MoviesServiceImpl:MoviesService{
    override fun getMoviesById(movieId: Long): Movie {
     return moviesDao.getMoviesById(movieId)

    }

    override fun getMovies(): List<Movie> {
        return moviesDao.getMovies()
    }
    @Autowired
    lateinit var moviesDao: MoviesDao
}