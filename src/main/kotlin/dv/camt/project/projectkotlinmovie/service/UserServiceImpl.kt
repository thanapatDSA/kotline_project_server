package dv.camt.project.projectkotlinmovie.service

import dv.camt.project.projectkotlinmovie.dao.UserDao
import dv.camt.project.projectkotlinmovie.entity.User
import dv.camt.project.projectkotlinmovie.entity.dto.UserDto
import dv.camt.project.projectkotlinmovie.entity.dto.UserLoginDto
import dv.camt.project.projectkotlinmovie.entity.dto.UserPasswordDto
import dv.camt.project.projectkotlinmovie.entity.dto.UserRegiserDto
import dv.camt.project.projectkotlinmovie.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class UserServiceImpl : UserService {
    override fun findUserById(id: Long): User {
        return userDao.findUserById(id)
    }

    override fun login(user: UserLoginDto): User {
        var userData = userDao.findUserByEmail(user.email)
        if (userData.password == user.password){
            return userData
        }else{
            return error("password is inveiln")
        }
    }

    override fun chagepassword(user: UserPasswordDto): User {
        val userEdit = UserRegiserDto(
                id = user.id,
                firstName = userDao.findUserById(user.id).firstName,
                lastName = userDao.findUserById(user.id).lastName,
                password = user.password,
                email = userDao.findUserById(user.id).email)
        val user = MapperUtil.INSTANCE.mapUserRegister(userEdit)
        return userDao.save(user)
    }

    override fun edit(user: UserDto): User {
        val userEdit = UserRegiserDto(
                id = user.id,
                firstName = user.firstName,
                lastName = user.lastName,
                password = userDao.findUserById(user.id).password,
                email = user.email)
        val user = MapperUtil.INSTANCE.mapUserRegister(userEdit)
        return userDao.save(user)
    }

    override fun save(user: UserRegiserDto): User {
        val user = MapperUtil.INSTANCE.mapUserRegister(user)
        return userDao.save(user)
    }

    @Autowired
    lateinit var userDao: UserDao

}