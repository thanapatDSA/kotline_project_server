package dv.camt.project.projectkotlinmovie.service

import dv.camt.project.projectkotlinmovie.entity.Booking

interface BookingService{
    fun booking(booking: Booking, showtimeId: Long, userId: Long): Booking
    fun getBooking(userId: Long): List<Booking>
}