package dv.camt.project.projectkotlinmovie.service

import dv.camt.project.projectkotlinmovie.entity.Cinema

interface CinemaService{
    fun getAllCinema(): List<Cinema>

}