package dv.camt.project.projectkotlinmovie.entity

import javax.persistence.*

@Entity
class SelectedSeat(@ManyToOne
                   var seatDetail: Seat? = null) {
    @Id
    @GeneratedValue
    var id: Long? = null
    @OneToMany
    var seats = mutableListOf<SeatShowtime>()
}