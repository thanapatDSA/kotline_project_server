package dv.camt.project.projectkotlinmovie.entity.dto

data class MovieDto(
        var id: Long? = null,
        var name: String? = null,
        var duration: Int? = null,
        var image: String? = null
)