package dv.camt.project.projectkotlinmovie.entity

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
class User(var firstName: String? = null,
           var lastName: String? = null,
           var email: String? = null,
           var password: String? = null) {
    @Id
    @GeneratedValue
    var id: Long? = null

}