package dv.camt.project.projectkotlinmovie.entity.dto

import dv.camt.project.projectkotlinmovie.entity.Seat

data class SelectedSeatDto(var seatDetail: Seat? = null)