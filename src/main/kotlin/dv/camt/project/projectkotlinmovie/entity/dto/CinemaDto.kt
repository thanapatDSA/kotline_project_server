package dv.camt.project.projectkotlinmovie.entity.dto

data class CinemaDto(var name: String? = null,
                     var seat: List<SeatDto> = mutableListOf())

