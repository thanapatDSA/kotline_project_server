package dv.camt.project.projectkotlinmovie.entity

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
class Seat(var name: String? = null,
           var price: Double? = null,
           @Column(name = "Seat_rows")
           var row: Int? = null,
           @Column(name = "Seat_columns")
           var columns: Int? = null,
           var type: SeatType? = null) {
    @Id
    @GeneratedValue
    var id: Long? = null
}