package dv.camt.project.projectkotlinmovie.entity

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.ManyToMany

@Entity
class Cinema(var name: String? = null){
    @Id
    @GeneratedValue
    var id: Long? = null
    @ManyToMany
    var seat = mutableListOf<Seat>()
}