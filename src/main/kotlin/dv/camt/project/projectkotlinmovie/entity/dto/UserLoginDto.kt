package dv.camt.project.projectkotlinmovie.entity.dto

data class UserLoginDto(var email: String? = null,
                        var password: String? = null)