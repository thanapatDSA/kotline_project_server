package dv.camt.project.projectkotlinmovie.entity.dto

import dv.camt.project.projectkotlinmovie.entity.SeatShowtime

data class BookingDto(var showtime: ShowTimeBookingDto? = null,
                      var creatDateTime: Long? = null,
                      var seats: List<SeatShowtime> = mutableListOf())