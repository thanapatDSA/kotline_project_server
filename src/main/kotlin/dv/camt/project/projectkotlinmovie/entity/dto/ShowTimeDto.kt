package dv.camt.project.projectkotlinmovie.entity.dto

import dv.camt.project.projectkotlinmovie.entity.SelectedSeat
import dv.camt.project.projectkotlinmovie.entity.Soundtrack
import dv.camt.project.projectkotlinmovie.entity.Subtitles


data class ShowTimeDto(
        var movie: MovieDto? = null,
        var id: Long? = null,
        var soundtrack: Soundtrack? = null,
        var subtitles: Subtitles? = null,
        var startDateTime: Long? = null,
        var endDateTime: Long? = null,
        var cinema: CinemaDto? = null
)