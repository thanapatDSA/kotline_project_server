package dv.camt.project.projectkotlinmovie.entity

import java.util.*
import javax.persistence.*

@Entity
class Booking (var creatDateTime: Long? = null
){
    @Id
    @GeneratedValue
    var id: Long? = null
    @ManyToOne
    var user: User? = null
    @ManyToMany
    var seats = mutableListOf<SeatShowtime>()
    @ManyToOne
    var showtime: Showtime? = null
}