package dv.camt.project.projectkotlinmovie.entity.dto

data class UserDto(
        var id: Long? = null,
        var firstName: String? = null,
        var lastName: String? = null,
        var email: String? = null
        )