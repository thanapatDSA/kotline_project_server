package dv.camt.project.projectkotlinmovie.entity

enum class SeatType{
    PREMIUM,SOFA,DELUXE
}