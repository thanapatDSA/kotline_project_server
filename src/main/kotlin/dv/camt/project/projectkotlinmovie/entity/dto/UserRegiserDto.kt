package dv.camt.project.projectkotlinmovie.entity.dto

data class UserRegiserDto(
        var id: Long? = null,
        var firstName: String? = null,
        var lastName: String? = null,
        var password: String? = null,
        var email: String? = null
)