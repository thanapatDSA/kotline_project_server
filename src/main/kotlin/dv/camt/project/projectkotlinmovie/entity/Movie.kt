package dv.camt.project.projectkotlinmovie.entity

import javax.persistence.*

@Entity
class Movie(
        var name: String? = null,
        var duration: Int? = null,
        var image: String? = null
){
    @Id
    @GeneratedValue
    var id: Long? = null
    @OneToMany(mappedBy = "movie")
    var showtimes = mutableListOf<Showtime>()
}
