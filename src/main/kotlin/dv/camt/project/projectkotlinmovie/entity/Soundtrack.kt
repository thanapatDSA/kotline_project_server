package dv.camt.project.projectkotlinmovie.entity

enum class Soundtrack {
    TH,EN,JP
}