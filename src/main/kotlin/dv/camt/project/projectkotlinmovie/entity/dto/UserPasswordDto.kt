package dv.camt.project.projectkotlinmovie.entity.dto

data class UserPasswordDto(var id: Long? = null,
                           var password: String? = null)