package dv.camt.project.projectkotlinmovie.entity

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
data class SeatShowtime(var status: Boolean = false,
                        @Column(name = "SeatShowtime_rows")
                        var row: String? = null,
                        @Column(name = "SeatShowtime_columns")
                        var column: String? = null) {
    @Id
    @GeneratedValue
    var id: Long? = null
}