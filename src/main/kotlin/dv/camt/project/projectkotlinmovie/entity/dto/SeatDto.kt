package dv.camt.project.projectkotlinmovie.entity.dto

import dv.camt.project.projectkotlinmovie.entity.SeatType

data class SeatDto (var name: String? = null,
                    var price: Double? = null,
                    var row: Int? = null,
                    var columns: Int? = null,
                    var type: SeatType? = null)
