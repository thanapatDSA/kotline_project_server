package dv.camt.project.projectkotlinmovie.entity

import java.time.LocalDate
import java.time.LocalDateTime
import java.util.*
import javax.persistence.*

@Entity
class Showtime(var startDateTime: Long? = null,
               var endDateTime: Long? = null,
               var soundtrack: Soundtrack? = null,
               var subtitles: Subtitles? = null){
    @Id
    @GeneratedValue
    var id: Long? = null
    @ManyToOne
    var movie: Movie? = null
    @OneToOne
    var cinema:Cinema? = null
    @OneToMany
    var seats = mutableListOf<SelectedSeat>()

    constructor( startDateTime: Long,
                 endDateTime: Long,
                 movie: Movie):
            this(startDateTime,endDateTime){
        this.movie = movie
    }
}