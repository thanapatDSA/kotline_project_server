package dv.camt.project.projectkotlinmovie

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class ProjectkotlinmovieApplication

fun main(args: Array<String>) {
    runApplication<ProjectkotlinmovieApplication>(*args)
}
