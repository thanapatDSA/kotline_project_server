package dv.camt.project.projectkotlinmovie.controller

import dv.camt.project.projectkotlinmovie.entity.Showtime
import dv.camt.project.projectkotlinmovie.service.MoviesService
import dv.camt.project.projectkotlinmovie.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController

@RestController
class MoviesController {
    @Autowired
    lateinit var moviesService: MoviesService

    @GetMapping("/movies")
    fun getAllMovies():ResponseEntity<Any>{
        val movies = moviesService.getMovies()
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapMovies(movies))
    }

    @GetMapping("/moviesRAW")
    fun getAllMoviesRAW():ResponseEntity<Any>{
        val movies = moviesService.getMovies()
        return ResponseEntity.ok(movies)
    }

    @GetMapping("/movies/{movieId}")
    fun getOneMovieShowTime(@PathVariable movieId: Long):ResponseEntity<Any>{
        val output = MapperUtil.INSTANCE.mapMovies(
                moviesService.getMoviesById(movieId)
        )
        return ResponseEntity.ok(output)
    }
}