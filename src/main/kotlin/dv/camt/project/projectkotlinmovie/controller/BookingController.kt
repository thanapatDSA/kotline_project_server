package dv.camt.project.projectkotlinmovie.controller

import dv.camt.project.projectkotlinmovie.entity.Booking
import dv.camt.project.projectkotlinmovie.service.BookingService
import dv.camt.project.projectkotlinmovie.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
class BookingController{
    @Autowired
    lateinit var bookingService: BookingService
    @PutMapping("/booking/{showtimeId}/{userId}")
    fun addBooking(@RequestBody booking: Booking,
                   @PathVariable showtimeId: Long,
                   @PathVariable userId: Long):ResponseEntity<Any>{
        val bookingTicket = bookingService.booking(booking,showtimeId,userId)
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapBooking(bookingTicket))
    }
    @GetMapping("/booking/user/{userId}")
    fun getBookingTickets(@PathVariable userId: Long):ResponseEntity<Any>{
        val tickets = bookingService.getBooking(userId)
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapBooking((tickets)))
    }

}