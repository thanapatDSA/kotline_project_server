package dv.camt.project.projectkotlinmovie.controller

import dv.camt.project.projectkotlinmovie.entity.dto.UserDto
import dv.camt.project.projectkotlinmovie.entity.dto.UserLoginDto
import dv.camt.project.projectkotlinmovie.entity.dto.UserPasswordDto
import dv.camt.project.projectkotlinmovie.entity.dto.UserRegiserDto
import dv.camt.project.projectkotlinmovie.service.UserService
import dv.camt.project.projectkotlinmovie.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
class UserController {
    @Autowired
    lateinit var userService: UserService

    @PostMapping("/user/register")
    fun addUserRegister(@RequestBody user: UserRegiserDto): ResponseEntity<Any> {
        return ResponseEntity.ok(
                MapperUtil.INSTANCE.mapUserRegister(
                        userService.save(user)
                )
        )
    }

    @PutMapping("/user/editprofile/")
    fun updateUserProfile(@RequestBody user: UserDto): ResponseEntity<Any> {
        if (user.id == null)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body("id mustnot be null")
        return ResponseEntity.ok(
                MapperUtil.INSTANCE.mapUser(
                        userService.edit(user)
                )
        )
    }

    @PutMapping("/user/chagepassword/")
    fun updateUserPassword(@RequestBody user: UserPasswordDto): ResponseEntity<Any> {
        if (user.id == null)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body("id mustnot be null")
        return ResponseEntity.ok(
                MapperUtil.INSTANCE.mapUser(
                        userService.chagepassword(user)
                )
        )
    }

    @PostMapping("/user/login")
    fun loginUser(@RequestBody user: UserLoginDto): ResponseEntity<Any>{
        if (user.email == null)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body("Email is not null")
        else if (user.password == null)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body("Password is not null")
        val login = MapperUtil.INSTANCE.mapUser(userService.login(user))
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapUser(login))
    }

    @GetMapping("/user/{userId}")
    fun getUser(@PathVariable userId:Long):ResponseEntity<Any>{
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapUser(userService.findUserById(userId)))
    }
}