package dv.camt.project.projectkotlinmovie.controller


import dv.camt.project.projectkotlinmovie.service.MoviesService
import dv.camt.project.projectkotlinmovie.service.ShowTimeService
import dv.camt.project.projectkotlinmovie.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController

@RestController
class ShowTimeController {
    @Autowired
    lateinit var showTimeService: ShowTimeService
    @Autowired
    lateinit var moviesService: MoviesService

    @GetMapping("/showtime")
    fun getAllShowTime():ResponseEntity<Any>{
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapShowTime(showTimeService.getAllShowtime()))
    }

    @GetMapping("/showtime/movie/{movieID}")
    fun getShowTimeById(@PathVariable("movieID") id: Long): ResponseEntity<Any> {
        val output = MapperUtil.INSTANCE.mapShowTime(
                showTimeService.getShowTimeByMovieId(id)
        )
        return ResponseEntity.ok(output)
    }
    @GetMapping("/showtime/{showtimeId}")
    fun getOneShowTimeById(@PathVariable showtimeId: Long): ResponseEntity<Any>{
        val output = MapperUtil.INSTANCE.mapShowTimeWithSeat(
                showTimeService.getShowTimeById(showtimeId)
        )
        return ResponseEntity.ok(output)
    }
}