package dv.camt.project.projectkotlinmovie.controller

import dv.camt.project.projectkotlinmovie.service.CinemaService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController


@RestController
class CinemaController {
    @Autowired
    lateinit var cinemaService: CinemaService

    @GetMapping("/cinema")
    fun getAllCinema():ResponseEntity<Any>{
        val output = cinemaService.getAllCinema()
        return ResponseEntity.ok(output)
    }
}