FROM openjdk:8-jdk-alpine

ADD build/libs/projectkotlinmovie-0.0.1-SNAPSHOT.jar app.jar
EXPOSE 8080
ENV JAVA_PROFILE deploy-db,db

ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom",\
    "-Dspring.profiles.active=${JAVA_PROFILE}",\
    "-jar","/app.jar"]
